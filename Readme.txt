README

This is a README for FOB's Extreme Automation course assignments.


What is this repository for?
This repository is for stashing automation course assignments.

First and second project tasks together with soruce code have been removed from the project as per latest task instructions.


Task 3
Description: Assignment for task 3 was to remove previous source code and create 3 new integration tests that communicate with Punk API.

Punk API: https://api.punkapi.com/
Certificate provider: 'Let's Encrypt Authority X3'

Steps
1. Clone project from repo
2. Run ’gradlew test’ command from root directory

Alternative steps
Due to the tests communicating with an external API over https you might encounter an ’sslhandshakeexception validatorexception’error when running the tests. In that case it is necessary to add and install the root certificate of the API to your java security folder and specify the java location when running the tests.

Installing the certificate:
1. Open https://api.punkapi.com/v2/beers/ from browser (Chrome)
2. Go to Chrome settings
3. Search for 'SSL' -> Open 'Manage certificates'
4. Find the correct cert under one of the tabs, 'Let's encrypt..'
5. Export cert to your java jre/lib/security folder
6. Run 'keytool -import -keystore cacerts -file yourcertname.cer' command in the same security folder
7. Once prompted type 'yes' in command line

Running the tests after this:
1. Move to project root folder
2. Run ’ gradlew test -Dorg.gradle.java.home="pathToYourJavaFolderWithCert"’ from command line


Who do I talk to?

Author: Oliver Oidekivi