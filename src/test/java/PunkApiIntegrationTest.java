import static org.junit.Assert.assertEquals;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

/**
 * Created by Oliver on 03.03.2017.
 */
public class PunkApiIntegrationTest {

  private static final String PUNK_API_BASE_URL = "https://api.punkapi.com/v2/";
  private String parameters = "";

  @Test
  public void usingBeerIdShouldReturnOneResult() throws IOException {

    parameters = "beers/1";
    HttpUriRequest request = new HttpGet(PUNK_API_BASE_URL + parameters);
    HttpResponse httpResponse = HttpClientBuilder
        .create()
        .build()
        .execute(request);

    String jsonString = EntityUtils.toString(httpResponse.getEntity());
    JSONArray jsonArray = new JSONArray(jsonString);

    assertEquals("ID returns a beer", 1, jsonArray.length());

  }

  @Test
  public void usingExistingBeerNameShouldReturnSearchedResult() throws IOException {

    parameters = "beers?beer_name=Arcade_Nation";
    HttpUriRequest request = new HttpGet(PUNK_API_BASE_URL + parameters);
    HttpResponse httpResponse = HttpClientBuilder
        .create()
        .build()
        .execute(request);

    String jsonString = EntityUtils.toString(httpResponse.getEntity());
    JSONArray jsonArray = new JSONArray(jsonString);

    JSONObject jsonobject = jsonArray.getJSONObject(0);
    String beerName = jsonobject.getString("name");

    assertEquals("Name returns specified beer", "Arcade Nation", beerName);

  }

  @Test
  public void usingParametersWithoutValuesShouldReturnBadRequestCode() throws IOException {

    parameters = "beers?beer_name";
    HttpUriRequest request = new HttpGet(PUNK_API_BASE_URL + parameters);
    HttpResponse httpResponse = HttpClientBuilder
        .create()
        .build()
        .execute(request);

    assertEquals("Empty parameter returns bad request code", HttpStatus.SC_BAD_REQUEST,
        httpResponse.getStatusLine().getStatusCode());

  }

}
